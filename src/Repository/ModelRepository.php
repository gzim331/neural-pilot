<?php

namespace App\Repository;

use App\Entity\Model;
use App\Entity\Training;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Model>
 *
 * @method Model|null find($id, $lockMode = null, $lockVersion = null)
 * @method Model|null findOneBy(array $criteria, array $orderBy = null)
 * @method Model[]    findAll()
 * @method Model[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModelRepository extends ServiceEntityRepository
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        ManagerRegistry $registry,
    )
    {
        parent::__construct($registry, Model::class);
    }

    public function getModelsToRun()
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.status IN (:status)')
            ->setParameter('status',[Model::STATUS_ADDED, Model::STATUS_CREATED])
            ->getQuery()
            ->getResult();
    }

    public function removeModel(Model $model): void
    {
        if ($model->getPath()) {
            unlink($model->getPath());
        }

        if (Model::STATUS_ADDED !== $model->getStatus()) {
            foreach ($this->entityManager->getRepository(Training::class)->findBy(['model' => $model]) as $training) {
                $this->entityManager->remove($training);
            }
        }

        $this->entityManager->remove($model);
        $this->entityManager->flush();
    }

    public function clearModel(Model $model): void
    {
        if ($model->getPath()) {
            unlink($model->getPath());
        }

        $model
            ->setPath(null)
            ->setClassNames(null);
    }

    public function updateModelStatus(Model $model, int $status): void
    {
        $model->setStatus($status);
        $this->entityManager->flush();
    }

    public function fillModelFields(Model $model, array $fields): void
    {
        if (isset($fields['name'])) {
            $model->setName($fields['name']);
        }
        if (isset($fields['className'])) {
            $model->setClassNames($fields['className']);
        }
        if (isset($fields['description'])) {
            $model->setDescription($fields['description']);
        }
        if (isset($fields['path'])) {
            $model->setPath($fields['path']);
        }
        if (isset($fields['status'])) {
            $model->setStatus($fields['status']);
        }
    }
}
