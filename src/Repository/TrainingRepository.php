<?php

namespace App\Repository;

use App\Entity\Dataset;
use App\Entity\Model;
use App\Entity\Training;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\InputBag;

/**
 * @extends ServiceEntityRepository<Training>
 *
 * @method Training|null find($id, $lockMode = null, $lockVersion = null)
 * @method Training|null findOneBy(array $criteria, array $orderBy = null)
 * @method Training[]    findAll()
 * @method Training[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrainingRepository extends ServiceEntityRepository
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        ManagerRegistry $registry,
    ) {
        parent::__construct($registry, Training::class);
    }

    public function fillTrainingFields(Training $training, InputBag $inputBag): void
    {
        $datasetId = (int)$inputBag->get('datasetIdInput');
        $epochsInput = (int)$inputBag->get('epochsInput');
        $batchSizeInput = (int)$inputBag->get('batchSizeInput');
        $activationInput = (string)$inputBag->get('activationInput');
        $optimizerInput = (string)$inputBag->get('optimizerInput');
        $imgSizeInput = (string)str_replace(' ', '', $inputBag->get('imgSizeInput'));
        $kernelsInput = (string)$inputBag->get('kernelsInput');
        $kernels = explode(',', $kernelsInput);

        $training
            ->setDataset($this->entityManager->getRepository(Dataset::class)->find($datasetId))
            ->setEpochs($epochsInput)
            ->setBatchSize($batchSizeInput)
            ->setActivation($activationInput)
            ->setOptimizer($optimizerInput)
            ->setImgSize($imgSizeInput)
            ->setKernels($kernels);
    }

    public function clearTrainingModel(Model $model): void
    {
        ($this->findOneBy(['model' => $model]))
            ->setResult(null)
            ->setStartRun(null)
            ->setEndRun(null);
    }
}
