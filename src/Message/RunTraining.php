<?php

namespace App\Message;

class RunTraining
{
    public function __construct(
        private int $trainingId,
    ) {
    }

    public function getTrainingId(): int
    {
        return $this->trainingId;
    }
}
