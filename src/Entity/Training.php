<?php

namespace App\Entity;

use App\Repository\TrainingRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TrainingRepository::class)]
class Training
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $epochs = null;

    #[ORM\Column]
    private ?int $batch_size = null;

    #[ORM\Column(length: 255)]
    private ?string $activation = null;

    #[ORM\Column(length: 255)]
    private ?string $optimizer = null;

    #[ORM\Column(length: 255)]
    private ?string $img_size = null;

    #[ORM\Column(type: Types::ARRAY)]
    private array $kernels = [];

    #[ORM\Column(nullable: true)]
    private ?array $result = null;

    #[ORM\ManyToOne(fetch: 'EAGER', inversedBy: 'trainings')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Model $model = null;

    #[ORM\ManyToOne(fetch: 'EAGER', inversedBy: 'trainings')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Dataset $dataset = null;

    #[ORM\ManyToOne(targetEntity: self::class)]
    private ?self $parent_training = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $startRun = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $endRun = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEpochs(): ?int
    {
        return $this->epochs;
    }

    public function setEpochs(int $epochs): static
    {
        $this->epochs = $epochs;

        return $this;
    }

    public function getBatchSize(): ?int
    {
        return $this->batch_size;
    }

    public function setBatchSize(int $batch_size): static
    {
        $this->batch_size = $batch_size;

        return $this;
    }

    public function getActivation(): ?string
    {
        return $this->activation;
    }

    public function setActivation(string $activation): static
    {
        $this->activation = $activation;

        return $this;
    }

    public function getOptimizer(): ?string
    {
        return $this->optimizer;
    }

    public function setOptimizer(string $optimizer): static
    {
        $this->optimizer = $optimizer;

        return $this;
    }

    public function getImgSize(): ?string
    {
        return $this->img_size;
    }

    public function setImgSize(string $img_size): static
    {
        $this->img_size = $img_size;

        return $this;
    }

    public function getKernels(): array
    {
        return $this->kernels;
    }

    public function setKernels(array $kernels): static
    {
        $this->kernels = $kernels;

        return $this;
    }

    public function getResult(): ?array
    {
        return $this->result;
    }

    public function setResult(?array $result): static
    {
        $this->result = $result;

        return $this;
    }

    public function getModel(): ?Model
    {
        return $this->model;
    }

    public function setModel(?Model $model): static
    {
        $this->model = $model;

        return $this;
    }

    public function getDataset(): ?Dataset
    {
        return $this->dataset;
    }

    public function setDataset(?Dataset $dataset): static
    {
        $this->dataset = $dataset;

        return $this;
    }

    public function getParentTraining(): ?self
    {
        return $this->parent_training;
    }

    public function setParentTraining(?self $parent_training): static
    {
        $this->parent_training = $parent_training;

        return $this;
    }

    public function getStartRun(): ?\DateTimeInterface
    {
        return $this->startRun;
    }

    public function setStartRun(?\DateTimeInterface $startRun): static
    {
        $this->startRun = $startRun;

        return $this;
    }

    public function getEndRun(): ?\DateTimeInterface
    {
        return $this->endRun;
    }

    public function setEndRun(?\DateTimeInterface $endRun): static
    {
        $this->endRun = $endRun;

        return $this;
    }
}
