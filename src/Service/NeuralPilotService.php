<?php

namespace App\Service;

use App\Entity\Training;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class NeuralPilotService
{
    public const MODELS_PATH = '/neural/MLmodels/models/';
    public const DATASETS_PATH = '/neural/MLmodels/datasets/';
    public const PYTHON_INTERPRETER = '/neural/neural_env/bin/python3';
    public const TRAIN_MODEL_SCRIPT = '/neural/MLmodels/train_model.py';
    public const RUN_MODEL_SCRIPT = '/neural/MLmodels/runModel.py';

    public function getResultModel(
        string $modelPath,
        string $itemPath,
        string $classNames,
        string $imageSize,
    ): string
    {
        $imgSize = explode(',', $imageSize);

        $process = new Process([
            self::PYTHON_INTERPRETER,
            self::RUN_MODEL_SCRIPT,
            $modelPath,
            $itemPath,
            $classNames,
            $imgSize[0],
            $imgSize[1]
        ]);

        try {
            $process->mustRun();
            if (preg_match("/Predicted result: (\w+)/", $process->getOutput(), $matches)) {
                return $matches[1];
            } else {
                return 'Not found';
            }
        } catch (ProcessFailedException $exception) {
            return 'An error occurred while running the script: ' . $exception->getMessage();
        }
    }

    public function deleteDirectory($dirPath): void
    {
        if (is_dir($dirPath)) {
            $files = scandir($dirPath);
            foreach ($files as $file) {
                if ($file !== '.' && $file !== '..') {
                    $filePath = $dirPath . '/' . $file;
                    if (is_dir($filePath)) {
                        $this->deleteDirectory($filePath);
                    } else {
                        unlink($filePath);
                    }
                }
            }
            rmdir($dirPath);
        }
    }

    public function beautifyResult(array $input): array|string|null
    {
        return explode("\n", trim($input[0]));
    }

    public function convertClasses(string $classes): string
    {
        $jsonStr = str_replace("'", '"', $classes);

        $data = json_decode($jsonStr, true);

        $result = [];
        foreach ($data as $key => $value) {
            $result[] = $value . ": " . $key;
        }

        return implode(", ", $result);
    }

    public function createProcessTraining(Training $training): Process
    {
        $imgSize = explode(',', $training->getImgSize());

        $process = new Process([
            NeuralPilotService::PYTHON_INTERPRETER,
            NeuralPilotService::TRAIN_MODEL_SCRIPT,
            'trainingId=' . $training->getId(),
            'epochs=' . $training->getEpochs(),
            'batch_size=' . $training->getBatchSize(),
            'img_height=' . $imgSize[0],
            'img_width=' . $imgSize[1],
            'activation=' . $training->getActivation(),
            'optimizer=' . $training->getOptimizer(),
            'filters={"amount": [' . implode(', ', $training->getKernels()) . ']}',
            'dataset_path=' . $training->getDataset()->getPath()
        ]);
        $process->setTimeout(864000); // 10 days

        return $process;
    }
}
