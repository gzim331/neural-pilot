<?php

namespace App\MessageHandler;

use App\Entity\Model;
use App\Message\RunTraining;
use App\Repository\ModelRepository;
use App\Repository\TrainingRepository;
use App\Service\NeuralPilotService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Process\Exception\ProcessFailedException;

#[AsMessageHandler]
class RunTrainingHandler
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly TrainingRepository $trainingRepository,
        private readonly ModelRepository $modelRepository,
        private readonly NeuralPilotService $neuralPilotService,
    ) {
    }

    public function __invoke(RunTraining $message): void
    {
        $training = $this->trainingRepository->find($message->getTrainingId());
        $model = $this->modelRepository->find($training->getModel()->getId());
        $process = $this->neuralPilotService->createProcessTraining($training);

        if (!$training) {
            throw new \Exception('Training not found');
        }

        $training->setStartRun(new \DateTime());
        $this->entityManager->flush();

        try {
            $process->mustRun();

            $modelFilePath = '';
            $patternFilePath = '/<MODEL_NAME_START>(.*?)<MODEL_NAME_END>/';
            preg_match_all($patternFilePath, $process->getOutput(), $matches);
            foreach ($matches[1] as $match) {
                $modelFilePath = NeuralPilotService::MODELS_PATH . $match;
            }

            $result = '';
            $patternResult = '/<START_RESULTS>(.*?)<END_RESULTS>/s';
            preg_match_all($patternResult, $process->getOutput(), $matches1);
            foreach ($matches1[1] as $match) {
                $result .= $match . "\n";
            }

            $classes = '';
            $patternClasses = '/<START_CLASS>(.*?)<END_CLASS>/s';
            preg_match_all($patternClasses, $process->getOutput(), $matches2);
            foreach ($matches2[1] as $match) {
                $classes .= $match . "\n";
            }

            $training->setResult([$result]);
            $model->setPath($modelFilePath);
            $model->setClassNames($this->neuralPilotService->convertClasses($classes));
            $model->setStatus(Model::STATUS_CREATED);

        } catch (ProcessFailedException $exception) {
            $training->setResult(['ERROR' => $exception->getMessage()]);
            $model->setStatus(Model::STATUS_ERROR);
        }

        $training->setEndRun(new \DateTime());

        $this->entityManager->flush();
    }
}
