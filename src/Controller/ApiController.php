<?php

namespace App\Controller;

use App\Entity\Model;
use App\Repository\ModelRepository;
use App\Repository\TrainingRepository;
use App\Service\NeuralPilotService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ApiController extends AbstractController
{
    public function __construct(
        private readonly ModelRepository $modelRepository,
        private readonly TrainingRepository $trainingRepository,
        private readonly NeuralPilotService $neuralPilotService,
    )
    {
    }

    #[Route('/api/model/details/{model_id}', name: 'api_get_model_details', methods: ['GET'])]
    public function getModelDetails(int $model_id): JsonResponse
    {
        $model = $this->modelRepository->find($model_id);

        $data = [
            'name' => $model->getName(),
            'description' => $model->getDescription(),
            'class_names' => $model->getClassNames(),
        ];

        if (Model::STATUS_CREATED === $model->getStatus()) {
            $training = $this->trainingRepository->findOneBy(['model' => $model_id]);
            $data['training'] = [
                'epochs' => $training->getEpochs(),
                'batch_size' => $training->getBatchSize(),
                'img_size' => $training->getImgSize(),
                'activation' => $training->getActivation(),
                'optimizer' => $training->getOptimizer(),
                'kernels' => $training->getKernels(),
                'result' => $training->getResult(),
            ];
        }

        return new JsonResponse([$data], Response::HTTP_OK);
    }

    #[Route('/api/model/run/{model_id}', name: 'api_run_model', methods: ['POST'])]
    public function runModel(int $model_id, Request $request): JsonResponse
    {
        $model = $this->modelRepository->find($model_id);
        $item = $request->files->get('itemInput');

        if (!$item) {
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }

        $destination = '/tmp/';
        $fileInfo = pathinfo($item->getClientOriginalName());
        $filename = $destination . $fileInfo['filename'] . '.' . $fileInfo['extension'];
        try {
            $item->move($destination, $filename);
        } catch (FileException) {
        }

        if (Model::STATUS_CREATED === $model->getStatus()) {
            $training = $this->trainingRepository->findOneBy(['model' => $model]);
            $result = $this->neuralPilotService->getResultModel(
                $model->getPath(),
                $filename,
                $model->getClassNames(),
                $training->getImgSize()
            );
        } else {
            $result = $this->neuralPilotService->getResultModel(
                $model->getPath(),
                $filename,
                $model->getClassNames(),
                '150,150'
            );
        }
        unlink($filename);

        return new JsonResponse(
            [
                'result' => $result
            ],
            Response::HTTP_OK
        );
    }
}
