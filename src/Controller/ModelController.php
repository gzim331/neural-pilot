<?php

namespace App\Controller;

use App\Entity\Dataset;
use App\Entity\Model;
use App\Entity\Training;
use App\Message\RunTraining;
use App\Repository\ModelRepository;
use App\Repository\TrainingRepository;
use App\Service\NeuralPilotService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Attribute\Route;

class ModelController extends AbstractController
{
    public function __construct(
        private readonly NeuralPilotService $neuralPilotService,
        private readonly EntityManagerInterface $entityManager,
        private readonly ModelRepository $modelRepository,
        private readonly TrainingRepository $trainingRepository,
    )
    {
    }

    #[Route('/', name: 'neural_pilot_index')]
    public function index(Request $request): Response
    {
        $models = $this->entityManager->getRepository(Model::class)->getModelsToRun();

        if ('' === $request->request->get('loadModel')) {
            $modelId = (int)$request->request->get('modelInput');
            $item = $request->files->get('itemInput');

            $model = $this->entityManager->getRepository(Model::class)->find($modelId);
            if ($item) {
                $destination = '/tmp/';
                $fileInfo = pathinfo($item->getClientOriginalName());
                $filename = $destination . $fileInfo['filename'] . '.' . $fileInfo['extension'];
                try {
                    $item->move($destination, $filename);
                } catch (FileException) {
                }

                if (Model::STATUS_CREATED === $model->getStatus()) {
                    $training = $this->trainingRepository->findOneBy(['model' => $model]);
                    $data = $this->neuralPilotService->getResultModel(
                        $model->getPath(),
                        $filename,
                        $model->getClassNames(),
                        $training->getImgSize()
                    );
                } else {
                    $data = $this->neuralPilotService->getResultModel(
                        $model->getPath(),
                        $filename,
                        $model->getClassNames(),
                        '150,150'
                    );
                }
                unlink($filename);
            }
        }

        return $this->render('neural_pilot/index.html.twig', [
            'models' => $models,
            'data' => $data ?? null
        ]);
    }

    #[Route('/add', name: 'neural_pilot_add')]
    public function addModel(Request $request): Response
    {
        if ('' === $request->request->get('submitInput')) {
            if ($file = $request->files->get('fileInput')) {
                $destination = NeuralPilotService::MODELS_PATH;
                $fileInfo = pathinfo($file->getClientOriginalName());
                $originalFilename = $fileInfo['filename'];
                $extensionFile = $fileInfo['extension'];
                $filename = md5(time() . $originalFilename) . '.' . $extensionFile;
                try {
                    $file->move($destination, $filename);

                    $model = new Model();
                    $this->modelRepository->fillModelFields(
                        $model,
                        [
                            'name' => $request->request->get('nameInput'),
                            'className' => $request->request->get('classNamesInput'),
                            'description' => $request->request->get('descriptionInput'),
                            'path' => $destination . $filename,
                            'status' => Model::STATUS_ADDED
                        ]
                    );
                    $this->entityManager->persist($model);
                    $this->entityManager->flush();

                    return $this->redirectToRoute('neural_pilot_list');
                } catch (FileException) {
                }
            }
        }

        return $this->render('neural_pilot/model-add.html.twig');
    }

    #[Route('/remove/{id}', name: 'neural_pilot_remove')]
    public function remove(int $id): RedirectResponse
    {
        $this->modelRepository->removeModel($this->entityManager->getRepository(Model::class)->find($id));

        return $this->redirectToRoute('neural_pilot_list');
    }

    #[Route('/list', name: 'neural_pilot_list')]
    public function listModels(): Response
    {
        return $this->render('neural_pilot/model_list.html.twig', [
            'models' => $this->entityManager->getRepository(Model::class)->findAll()
        ]);
    }

    #[Route('/edit-added-model/{id}', name: 'neural_pilot_edit-added-model')]
    public function detailsAddedModel(Request $request, int $id): Response
    {
        $model = $this->entityManager->getRepository(Model::class)->find($id);
        if ('' === $request->request->get('submitInput')) {
            $this->modelRepository->fillModelFields(
                $model,
                [
                    'name' => $request->request->get('nameInput'),
                    'className' => $request->request->get('classNamesInput'),
                    'description' => $request->request->get('descriptionInput'),
                ]
            );
            $this->entityManager->flush();
        }

        return $this->render('neural_pilot/edit.html.twig', [
            'model' => $model
        ]);
    }

    #[Route('/create-model', name: 'neural_pilot_create_model')]
    public function createModel(Request $request, MessageBusInterface $bus): Response
    {
        if ('' !== $request->request->get('submitInput')) {
            return $this->render('neural_pilot/model-create.html.twig', [
                'datasets' => $this->entityManager->getRepository(Dataset::class)->findAll()
            ]);
        }

        $model = new Model();
        $this->modelRepository->fillModelFields(
            $model,
            [
                'name' => md5(time()),
                'status' => Model::STATUS_TRAINING
            ]
        );
        $this->entityManager->persist($model);

        $training = (new Training())
            ->setModel($model);
        $this->trainingRepository->fillTrainingFields($training, $request->request);

        $this->entityManager->persist($training);
        $this->entityManager->flush();

        $bus->dispatch(new RunTraining(
            $training->getId()
        ));

        return $this->redirectToRoute('neural_pilot_list');
    }

    #[Route('/edit-created-model/{id}', name: 'neural_pilot_edit-created-model')]
    public function detailsCreatedModel(Request $request, int $id, MessageBusInterface $bus): Response
    {
        $model = $this->entityManager->getRepository(Model::class)->find($id);
        $training = $this->entityManager->getRepository(Training::class)->findOneBy(['model' => $model]);

        if ('' === $request->request->get('submitEditModelInput')) {
            $this->modelRepository->fillModelFields(
                $model,
                [
                    'name' => $request->request->get('nameInput'),
                    'className' => $request->request->get('classNamesInput'),
                    'description' => $request->request->get('descriptionInput'),
                ]
            );
            $this->entityManager->flush();
        }

        if ('' === $request->request->get('submitTrainAgainInput')) {
            $this->modelRepository->clearModel($model);
            $this->trainingRepository->clearTrainingModel($model);
            $this->trainingRepository->fillTrainingFields($training, $request->request);
            $this->modelRepository->updateModelStatus($model, Model::STATUS_TRAINING);

            $bus->dispatch(new RunTraining(
                $training->getId()
            ));

            return $this->redirectToRoute('neural_pilot_list');
        }

        $result = $training->getResult();
        if ($result) {
            try {
                $result = $this->neuralPilotService->beautifyResult($training->getResult());
            } catch (\Exception) {
            }
        }

        return $this->render('neural_pilot/edit-trained.html.twig', [
            'model' => $model,
            'datasets' => $this->entityManager->getRepository(Dataset::class)->findAll(),
            'training' => $training,
            'result' => $result ?? null
        ]);
    }

    #[Route('/train-again/{id}', name: 'neural_pilot_train_again')]
    public function trainAgain(int $id, MessageBusInterface $bus): RedirectResponse
    {
        $training = $this->entityManager->getRepository(Training::class)->findOneBy(['model' => $id]);

        if (Model::STATUS_TRAINING === $training->getModel()->getStatus()) {
            return $this->redirectToRoute('neural_pilot_list');
        }

        $model = $this->modelRepository->find($training->getModel()->getId());

        $this->modelRepository->clearModel($model);
        $this->trainingRepository->clearTrainingModel($model);
        $this->modelRepository->updateModelStatus($model, Model::STATUS_TRAINING);

        $bus->dispatch(new RunTraining(
            $training->getId()
        ));

        return $this->redirectToRoute('neural_pilot_list');
    }

    #[Route('/cancel-training/{id}', name: 'neural_pilot_cancel_training')]
    public function cancelTraining(int $id, MessageBusInterface $bus): RedirectResponse
    {
        $model = $this->entityManager->getRepository(Model::class)->find($id);

        if (Model::STATUS_CANCEL === $model->getStatus()) {
            return $this->redirectToRoute('neural_pilot_list');
        }

        $this->trainingRepository->clearTrainingModel($model);
        $this->modelRepository->updateModelStatus($model, Model::STATUS_CANCEL);

        return $this->redirectToRoute('neural_pilot_list');
    }
}
