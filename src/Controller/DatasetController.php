<?php

namespace App\Controller;

use App\Entity\Dataset;
use App\Service\NeuralPilotService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class DatasetController extends AbstractController
{
    public function __construct(
        private readonly NeuralPilotService $neuralPilotService,
        private readonly EntityManagerInterface $entityManager,
    )
    {
    }

    #[Route('/list-dataset', name: 'neural_pilot_list_dataset')]
    public function listDatasets(): Response
    {
        return $this->render('neural_pilot/dataset/select.html.twig', [
            'datasets' => $this->entityManager->getRepository(Dataset::class)->findAll()
        ]);
    }

    #[Route('/create-dataset', name: 'neural_pilot_create_dataset')]
    public function createDataset(Request $request): Response
    {
        if ('' === $request->request->get('submitInput')) {
            $nameInput = $request->request->get('nameInput');

            if ($file = $request->files->get('fileInput')) {
                $destination = NeuralPilotService::DATASETS_PATH;
                $fileInfo = pathinfo($file->getClientOriginalName());
                $filename = $fileInfo['filename'];
                try {
                    $newFolderName = uniqid();
                    $zip = new \ZipArchive();

                    if (true === $zip->open($file->getPathname())) {
                        $zip->extractTo($destination);
                        $zip->close();

                        $originalFolderPath = $destination . $filename;
                        $newFolderPath = $destination . $newFolderName;

                        rename($originalFolderPath, $newFolderPath);

                        $dataset = (new Dataset())
                            ->setName($nameInput)
                            ->setPath($newFolderPath);
                        $this->entityManager->persist($dataset);
                        $this->entityManager->flush();

                        return $this->redirectToRoute('neural_pilot_list_dataset');
                    }
                } catch (FileException) {
                }
            }
        }

        return $this->render('neural_pilot/dataset/create.html.twig');
    }

    #[Route('/remove-dataset/{id}', name: 'neural_pilot_remove-dataset')]
    public function removeDataset(int $id): RedirectResponse
    {
        $dataset = $this->entityManager->getRepository(Dataset::class)->find($id);
        $this->neuralPilotService->deleteDirectory($dataset->getPath());

        $this->entityManager->remove($dataset);
        $this->entityManager->flush();

        return $this->redirectToRoute('neural_pilot_list_dataset');
    }
}
