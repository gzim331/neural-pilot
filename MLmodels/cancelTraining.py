import mysql.connector
from mysql.connector import Error
import subprocess
import time


def basic_check_process():
    try:
        pattern = "/neural/neural_env/bin/python3 /neural/MLmodels/train_model.py"
        result = subprocess.run(['pgrep', '-f', pattern], text=True, capture_output=True)
        if result.stdout:
            return True
    except subprocess.CalledProcessError:
        pass
    return False


def check_process(training_id):
    try:
        pattern = f"/neural/neural_env/bin/python3 /neural/MLmodels/train_model.py trainingId={training_id}"
        result = subprocess.run(['pgrep', '-f', pattern], text=True, capture_output=True)
        if result.stdout:
            pid = result.stdout.strip()
            subprocess.run(['kill', pid], text=True, capture_output=True)
    except subprocess.CalledProcessError:
        pass


def connect_fetch():
    try:
        connection = mysql.connector.connect(
            host='mysql',
            database='neural',
            user='neural',
            password='root'
        )

        if connection.is_connected():
            cursor = connection.cursor()
            query_model = "SELECT id FROM model WHERE status = 5"
            cursor.execute(query_model)
            model_ids = cursor.fetchall()

            if model_ids:
                for (model_id,) in model_ids:
                    query_training = "SELECT id FROM training WHERE model_id = %s"
                    cursor.execute(query_training, (model_id,))
                    training_ids = cursor.fetchall()

                    if training_ids:
                        for (training_id,) in training_ids:
                            check_process(training_id)

    except Error:
        pass
    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()


if __name__ == '__main__':
    while True:
        if basic_check_process():
            connect_fetch()
        time.sleep(5)
