import numpy as np
import sys
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import image


def load_and_prepare_image(image_path, image_height, image_width):
    img = image.load_img(image_path, target_size=(image_height, image_width))
    img_array = image.img_to_array(img)
    img_array_expanded_dims = np.expand_dims(img_array, axis=0)
    return img_array_expanded_dims


def predict_coffee_species(model_path, image_path, class_names, image_height, image_width):
    model = load_model(model_path)
    img = load_and_prepare_image(image_path, image_height, image_width)
    predictions = model.predict(img)
    predicted_class = np.argmax(predictions, axis=1)
    predicted_class_name = class_names[predicted_class[0]]
    return predicted_class_name


if __name__ == "__main__":
    if len(sys.argv) > 1:
        model_path = sys.argv[1]
        image_path = sys.argv[2]
        class_names = [item.split(': ')[1] for item in sys.argv[3].split(', ')]
        image_height = int(sys.argv[4])
        image_width = int(sys.argv[5])
        predicted_class_name = predict_coffee_species(model_path, image_path, class_names, image_height, image_width)
        print(f"Predicted result: {predicted_class_name}")
    else:
        print("Please provide an image path.")
