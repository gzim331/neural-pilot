#!/bin/bash

SCRIPT_PATH="/neural/MLmodels/cancelTraining.py"
PATTERN="/neural/neural_env/bin/python3 ${SCRIPT_PATH}"
PID=$(pgrep -f "${PATTERN}")

if [ -z "$PID" ]; then
    nohup /neural/neural_env/bin/python3 ${SCRIPT_PATH} &> /dev/null &
else
    kill -9 $PID
fi
