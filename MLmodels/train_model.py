import time
from tensorflow.keras.preprocessing import image_dataset_from_directory
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Flatten, Dense, Input, LeakyReLU, PReLU
from tensorflow.keras.models import Model
import numpy as np
from sklearn.metrics import f1_score, precision_score, recall_score
import tensorflow as tf
from sklearn.metrics import roc_auc_score
from tensorflow.keras.utils import to_categorical
import matplotlib.pyplot as plt
from contextlib import redirect_stdout
import sys
import json
import hashlib
from tensorflow.keras.preprocessing.image import ImageDataGenerator

params = {
    "trainingId": 1,
    "epochs": 20,
    "batch_size": 32,
    "img_height": 150,
    "img_width": 150,
    "activation": "relu",
    "optimizer": "adam",
    "loss": "categorical_crossentropy",
    "filters": {
        "amount": [16, 32, 64],
    },
    "dataset_path": "./datasets/"
}

sufix_name = ""

if __name__ == "__main__":
    arguments = sys.argv[1:]

    for arg in arguments:
        if '=' in arg:
            key, value = arg.split('=', 1)
            sufix_param = key
            if key == "filters":
                try:
                    params[key] = json.loads(value.replace("'", "\""))
                    sufix_value = ','.join(str(number) for number in params[key]["amount"])
                except json.JSONDecodeError as e:
                    pass
            elif key in params:
                params[key] = value
                sufix_value = value
            sufix_name = "-" + sufix_name + sufix_param + "-" + sufix_value

# Settings
data_directory = params["dataset_path"]
img_height = int(params['img_height'])
img_width = int(params['img_width'])
batch_size = int(params["batch_size"])
epochs = int(params["epochs"])
activation = params["activation"]
optimizer = params["optimizer"]
loss = params["loss"]
filters = params["filters"]["amount"]

# Load data
train_dataset = image_dataset_from_directory(
    f'{data_directory}/train',
    label_mode='categorical',
    image_size=(img_height, img_width),
    batch_size=batch_size,
    shuffle=False
)

validation_dataset = image_dataset_from_directory(
    f'{data_directory}/valid',
    label_mode='categorical',
    image_size=(img_height, img_width),
    batch_size=batch_size,
    shuffle=False
)

test_dataset = image_dataset_from_directory(
    f'{data_directory}/test',
    label_mode='categorical',
    image_size=(img_height, img_width),
    batch_size=batch_size,
    shuffle=False
)

num_classes = len(train_dataset.class_names)

model = Sequential()
model.add(Input(shape=(img_height, img_width, 3)))

for filter in filters:
    model.add(Conv2D(filter, (3, 3), activation=activation))
    model.add(MaxPooling2D(2, 2))

model.add(Flatten())
model.add(Dense(filters[-1], activation=activation))
model.add(Dense(num_classes, activation='softmax'))

model.compile(optimizer=optimizer, loss=loss, metrics=['accuracy'])

# Training model
start_time = time.time()

history = model.fit(
    train_dataset,
    epochs=epochs,
    validation_data=validation_dataset
)

total_time = time.time() - start_time


# Calculating metrics
def get_true_predictions(test_dataset, model):
    y_true = np.concatenate([y for x, y in test_dataset], axis=0)
    y_pred = model.predict(test_dataset)
    y_pred = np.argmax(y_pred, axis=1)
    y_true = np.argmax(y_true, axis=1)
    return y_true, y_pred


# Model evaluation on test data
test_loss, test_accuracy = model.evaluate(test_dataset)

# Predicting probabilities
y_pred_proba = model.predict(test_dataset)

y_true, y_pred = get_true_predictions(test_dataset, model)

# Transforming y_true to categorical form (one-hot encoding)
y_true_categorical = to_categorical(y_true, num_classes=num_classes)

# Calculating the AUC ROC for each class and calculating the average
roc_auc = roc_auc_score(y_true_categorical, y_pred_proba, multi_class='ovr', average='macro')

precision = precision_score(y_true, y_pred, average='macro', zero_division=0)
recall = recall_score(y_true, y_pred, average='macro', zero_division=0)
f1 = f1_score(y_true, y_pred, average='macro', zero_division=0)

string_to_encode = f'{time.time()}{test_accuracy}{precision}{recall}{f1}{roc_auc}'
modelFilename = hashlib.md5(string_to_encode.encode('utf-8')).hexdigest()
model.save(f'/neural/MLmodels/models/{modelFilename}.keras')

print(f'<MODEL_NAME_START>{modelFilename}.keras<MODEL_NAME_END>')

print('<START_RESULTS>')
print(f"Test accuracy: {test_accuracy}\n Precision: {precision}\n Recall: {recall}\n F1 Score: {f1}\n ROC AUC Score: {roc_auc}\n Time to learn: {round(total_time, 1)} seconds\n\n")
# print(model.summary())
print('<END_RESULTS>')

train_datagen = ImageDataGenerator()
train_generator = train_datagen.flow_from_directory(
    f'{data_directory}/train'
)

print(f'<START_CLASS>{train_generator.class_indices}<END_CLASS>')
